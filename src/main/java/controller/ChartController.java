package controller;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.OHLCDataset;
import utils.DataFeeder;
import java.net.URISyntaxException;

public class ChartController {
    private DataFeeder feeder;
    private JFreeChart chart;

    public ChartController(){
        try {
            feeder = new DataFeeder("kgh_d.csv");

        } catch (URISyntaxException e) {
            //todo: handle exception (after adding custom file reading) or populate data with zero value element
            e.printStackTrace();
        }

        chart = createChart(feeder.getDataSet(), feeder.getFileName());

    }

    private static JFreeChart createChart(OHLCDataset dataset, String title){

        //todo: display filename in title or stock name in case of database access
        return ChartFactory.createCandlestickChart(title,
                "time",
                "price",
                dataset,
                false);
    }

    public JFreeChart getChart() {
        return chart;
    }
}
