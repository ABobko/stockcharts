package properties;

//not used yet (for future improvements)
public class ConnectionProperties {
    private String user;
    private String password;
    private String urlDataBase;

    ConnectionProperties(String user, String password, String urlDataBase){
        this.user = user;
        this.password = password;
        this.urlDataBase = urlDataBase;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getUrlDataBase() {
        return urlDataBase;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUrlDataBase(String urlDataBase) {
        this.urlDataBase = urlDataBase;
    }

}
