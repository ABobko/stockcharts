package view;

import controller.ChartController;
import org.jfree.chart.ChartPanel;
import javax.swing.*;
import java.awt.*;

public class PrimaryWindow extends JFrame {

    ChartController controller;
    public PrimaryWindow(String title){
        super(title);

        controller = new ChartController();

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel swingPanel = new JPanel();
        swingPanel.setLayout(new BorderLayout());

        ChartPanel chartPanel = new ChartPanel(controller.getChart());
        swingPanel.add(chartPanel, BorderLayout.CENTER);
        swingPanel.setVisible(true);
        swingPanel.validate();

        add(swingPanel);
        pack();
        setVisible(true);
    }

}
