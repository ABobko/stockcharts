package utils;

import org.jfree.data.xy.DefaultOHLCDataset;
import org.jfree.data.xy.OHLCDataItem;
import org.jfree.data.xy.OHLCDataset;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

public class DataFeeder {
    private Path fileData;
    private SimpleDateFormat standardDateFormat;
    private OHLCDataset dataSet;
    private List<OHLCDataItem> dataItems;
    private String fileName;

    public DataFeeder(String fileName) throws URISyntaxException{
        this.fileName = fileName;
        dataItems = new ArrayList<>();
        standardDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        URI fileDataURI = DataFeeder.class.getResource("/" + fileName).toURI();
        fileData = Paths.get(fileDataURI);
        this.prepreOHLCSeries();
    }

    private void prepreOHLCSeries() {
        try{
            List<String> lines = Files.readAllLines(fileData);

            lines.forEach(line -> {
                try {
                    dataItems.add(prepareOHLCDataItem(line));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            });

            OHLCDataItem data[] = dataItems.toArray(new OHLCDataItem[dataItems.size()]);
            dataSet = new DefaultOHLCDataset("",data);

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private OHLCDataItem prepareOHLCDataItem(String line) throws ParseException {

        StringTokenizer tokenizer = new StringTokenizer(line, ",");

        Date date = standardDateFormat.parse(tokenizer.nextToken());
        double open = Double.parseDouble(tokenizer.nextToken());
        double high = Double.parseDouble(tokenizer.nextToken());
        double low = Double.parseDouble(tokenizer.nextToken());
        double close = Double.parseDouble(tokenizer.nextToken());
        double volume = Double.parseDouble(tokenizer.nextToken());

        return new OHLCDataItem(date, open, high, low, close, volume);
    }

    public void showDataItems(){
        dataItems.forEach(item -> System.out.println("open: "+item.getOpen()+", " +
                "high: "+item.getHigh()+", "+
                "low: "+item.getLow()+", "+
                "close: "+item.getClose()+", "+
                "volume: "+item.getVolume()
        ));
    }

    public OHLCDataset getDataSet(){
        return dataSet;
    }

    public String getFileName(){
        return fileName;
    }

}
